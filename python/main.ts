//% color="#4169E1" iconWidth=50 iconHeight=40
namespace Weather{

    //% block="Seniverse init Api [API]" blockType="command" 
    //% API.shadow="normal" API.defl="your_private_key"
    export function Weather(parameter: any, block: any) {
        let api = parameter.API.code;

        Generator.addImport("import seniverse");
        Generator.addInit(`api`,`api = "${api}"`);
        Generator.addInit(`lang`,`lang = ""`);
        Generator.addInit(`temp_unit`,`temp_unit = ""`);
 
    }

    
    //% block="Seniverse init Api [API]Lang [LANG]Tem [TEM]" blockType="command" 
    //% API.shadow="normal" API.defl="your_private_key"
    //% LANG.shadow="dropdown" LANG.options="LANG"
    //% TEM.shadow="dropdown" TEM.options="TEM"
    export function Weather0(parameter: any, block: any) {
        let api = parameter.API.code;
        let lang = parameter.LANG.code;
        let temp_unit = parameter.TEM.code;

        Generator.addImport("import seniverse");
        Generator.addInit(`api`,`api = "${api}"`);
        Generator.addInit(`lang`,`lang = "${lang}"`);
        Generator.addInit(`temp_unit`,`temp_unit = "${temp_unit}"`);
 
    }

    //% block="---"
    export function noteSep1() {

    }

    //% block="Seniverse[MESS] Get city [COUNTRY][WEATHER]" blockType="command" 
    //% MESS.shadow="normal" MESS.defl="weather"
    //% WEATHER.shadow="dropdown" WEATHER.options="WEATHER"
    //% COUNTRY.shadow="normal" COUNTRY.defl="上海"
    export function Weather1(parameter: any, block: any) {
        let mess = parameter.MESS.code;
        let weather = parameter.WEATHER.code;
        let country = parameter.COUNTRY.code;

   
        if(`${weather}`===`suggestion`){

            Generator.addCode(`url = "https://api.seniverse.com/v3/life/${weather}.json?key=" + api + lang + temp_unit`)
            Generator.addCode(`${mess} = seniverse.get_seni_weather(url ,"${country}")`);
           
        }else {

            Generator.addCode(`url = "https://api.seniverse.com/v3/weather/${weather}.json?key=" + api + lang + temp_unit`)
            Generator.addCode(`${mess} = seniverse.get_seni_weather(url ,"${country}")`);

        }

    }

    //% block="Seniverse[MESS] General field[NAME]  " blockType="reporter" 
    //% MESS.shadow="normal" MESS.defl="weather"  
    //% NAME.shadow="dropdown" NAME.options="NAME"   
    export function Weather2(parameter: any, block: any) {
        let mess = parameter.MESS.code;
        let name = parameter.NAME.code;  
         
        if(`${name}`===`:10`){
            Generator.addCode(`${mess}["results"][0]["last_update"][${name.replace("\"","").replace("\"","")}]`);
 
        }else if(`${name}`===`date`){
            Generator.addCode(`${mess}["results"][0]["last_update"]`);
 
        }else if(`${name}`===`object`){
            Generator.addCode(`${mess}`);
 
        }else {
            Generator.addCode(`${mess}["results"][0]["location"]["${name}"]`);
        
        }   
    }

    //% block="---"
    export function noteSep2() {

    }

    //% block="Seniverse[MESS] WEATHER Brief [NOW]" blockType="reporter" 
    //% MESS.shadow="normal" MESS.defl="weather"
    //% NOW.shadow="dropdown" NOW.options="NOW"   
    export function Weather3(parameter: any, block: any) {
        let mess = parameter.MESS.code; 
        let now = parameter.NOW.code;  
    
        Generator.addCode(`${mess}["results"][0]["now"]["${now}"]`);
           
    }
 
    //% block="Seniverse[MESS] WEATHER Day [DAY]Temp [TEMP]" blockType="reporter" 
    //% MESS.shadow="normal" MESS.defl="weather"
    //% DAY.shadow="dropdown" DAY.options="DAY"
    //% TEMP.shadow="dropdown" TEMP.options="TEMP"
    export function Weather4(parameter: any, block: any) {
        let mess = parameter.MESS.code;
        let day = parameter.DAY.code;  
        let temp = parameter.TEMP.code;
    
       Generator.addCode(`${mess}["results"][0]["daily"][${day}]["${temp}"]`);
    }
 
    //% block="Seniverse[MESS] WEATHER Brief [BRIEF]" blockType="reporter" 
    //% MESS.shadow="normal" MESS.defl="weather"
    //% BRIEF.shadow="dropdown" BRIEF.options="BRIEF"   
    export function Weather5(parameter: any, block: any) {
        let mess = parameter.MESS.code;
        let brief = parameter.BRIEF.code;  
             
        Generator.addCode(`${mess}["results"][0]["suggestion"]["${brief}"]["brief"]`);
                  
    }
}
    