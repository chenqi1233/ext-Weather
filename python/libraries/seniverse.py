import json
import requests


def get_seni_weather(_url, _location):
    try:
        _url = _url + "&location=" + _location.replace(" ", "%20")
        response = requests.get(_url)
        json_data = response.json()
        response.raise_for_status()  # 检查响应状态码，如果不是200，则抛出异常
        response.close()
        # return json_data

    except requests.exceptions.RequestException as e:
        error_code = json_data["status_code"]

        if error_code == "AP010001" :
            print("请求出错: " + "API 请求参数错误。")
        elif error_code == "AP010002" :
            print("请求出错: " + "没有权限访问这个 API 接口。")
        elif error_code == "AP010003" :
            print("请求出错: " + "API 密钥 key 错误。")
        elif error_code == "AP010004" :
            print("请求出错: " + "签名错误。")
        elif error_code == "AP010005" :
            print("请求出错: " + "你请求的 API 不存在。")
        elif error_code == "AP010006" :
            print("请求出错: " + "没有权限访问这个地点。")
        elif error_code == "AP010007" :
            print("请求出错: " + "JSONP 请求需要使用签名验证方式。")
        elif error_code == "AP010008" :
            print("请求出错: " + "没有绑定域名。请在 控制台 对应的产品管理下进行域名绑定。")
        elif error_code == "AP010009" :
            print("请求出错: " + "API 请求的 user-agent 与你设置的不一致。")
        elif error_code == "AP010010" :
            print("请求出错: " + "没有这个地点。")
        elif error_code == "AP010011" :
            print("请求出错: " + "无法查找到指定 IP 地址对应的城市。")
        elif error_code == "AP010012" :
            print("请求出错: " + "你的服务已经过期。")
        elif error_code == "AP010013" :
            print("请求出错: " + "访问量余额不足。")
        elif error_code == "AP010014" :
            print("请求出错: " + "访问频率超过限制。")
        elif error_code == "AP010015" :
            print("请求出错: " + "暂不支持该城市的车辆限行信息。")
        elif error_code == "AP010016" :
            print("请求出错: " + "暂不支持该城市的潮汐数据。")
        elif error_code == "AP010017" :
            print("请求出错: " + "请求的坐标超出支持的范围。")
        elif error_code == "AP100001" :
            print("请求出错: " + "系统内部错误：数据缺失。")
        elif error_code == "AP100002" :
            print("请求出错: " + "系统内部错误：数据错误。")
        elif error_code == "AP100003" :
            print("请求出错: " + "系统内部错误：服务内部错误。")
        elif error_code == "AP100004" :
            print("请求出错: " + "系统内部错误：网关错误。")
    except json.JSONDecodeError as e:
        print("解析JSON出错:", e)
    except Exception as e:
        print("发生未知错误:", e)
    return json_data
