# 心知天气
可实时监测天气

![](./micropython/_images/featured.png)

# 积木

![](./micropython/_images/blocks.png)

# 程序实例

# micropython示例

![](./micropython/_images/example1.png)


![](./micropython/_images/example.png)

![](./micropython/_images/example2.png)

# python示例

![](./python/_images/example.png)


# 支持列表

|主板型号|实时模式|ArduinoC|MicroPython|Python|备注|
|-----|-----|:-----:|:-----:|:-----:|-----|
|mpython|||√|||
|行空板||||√|||


# 更新日志

V0.0.1 基础功能完成

V0.0.2 支持python模式