//% color="#4169E1" iconWidth=50 iconHeight=40
namespace Weather{
    //% block="WEATHER Init Mess [MESS]Weather [WEATHER]Country [COUNTRY]Api [API]Lau [LAU]Tem [TEM]" blockType="command" 
    //% MESS.shadow="string" MESS.defl="w1"
    //% WEATHER.shadow="dropdown" WEATHER.options="WEATHER"
    //% COUNTRY.shadow="string" COUNTRY.defl="shanghai"
    //% API.shadow="string" API.defl="your_private_key"
    //% LAU.shadow="dropdown" LAU.options="LAU"
    //% TEM.shadow="dropdown" TEM.options="TEM"
    export function Weather(parameter: any, block: any) {
         let mess = parameter.MESS.code;  
         let weather = parameter.WEATHER.code;
         let country = parameter.COUNTRY.code;
         let api = parameter.API.code;
         let lau = parameter.LAU.code;
         let tem = parameter.TEM.code;
       
        Generator.addImport("import json");
        Generator.addImport("import urequests");
        
        Generator.addCode(`def get_seni_weather(_url, _location):`);
        Generator.addCode(`    _url = _url + "&location=" + _location.replace(" ", "%20")`);
        Generator.addCode(`    response = urequests.get(_url)`);
        Generator.addCode(`    json = response.json()`);
        Generator.addCode(`    response.close()`);
        Generator.addCode(`    return json`);
   
       if(`${weather}`===`suggestion`){
            
        Generator.addCode(`${mess.replace("\"","").replace("\"","")} = get_seni_weather("https://api.seniverse.com/v3/life/${weather}.json?key=${api.replace("\"","").replace("\"","")}${lau}${tem}","${country.replace("\"","").replace("\"","")}")`);
           
       }else {
        Generator.addCode(`${mess.replace("\"","").replace("\"","")} = get_seni_weather("https://api.seniverse.com/v3/weather/${weather}.json?key=${api.replace("\"","").replace("\"","")}${lau}${tem}","${country.replace("\"","").replace("\"","")}")`);


       }
    }
    //% block="WEATHER  Mess [MESS] Name[NAME]  " blockType="reporter" 
    //% MESS.shadow="string" MESS.defl="w1"  
    //% NAME.shadow="dropdown" NAME.options="NAME"   
    export function Weather2(parameter: any, block: any) {
        let mess = parameter.MESS.code; 
        let name = parameter.NAME.code;  
         
        if(`${name}`===`:10`){
            Generator.addCode(`${mess.replace("\"","").replace("\"","")}["results"][0]["last_update"][${name.replace("\"","").replace("\"","")}]`);
 
        }else if(`${name}`===`date`){
            Generator.addCode(`${mess.replace("\"","").replace("\"","")}["results"][0]["last_update"]`);
 
        }else if(`${name}`===`object`){
            Generator.addCode(`${mess.replace("\"","").replace("\"","")}`);
 
        }else {
            Generator.addCode(`${mess.replace("\"","").replace("\"","")}["results"][0]["location"]["${name}"]`);
        
        }   
   }
    //% block="WEATHER  Mess[MESS] Brief [NOW]  " blockType="reporter" 
    //% MESS.shadow="string" MESS.defl="w1"  
    //% NOW.shadow="dropdown" NOW.options="NOW"   
    export function Weather4(parameter: any, block: any) {
        let mess = parameter.MESS.code; 
        let now = parameter.NOW.code;  
    
        Generator.addCode(`${mess.replace("\"","").replace("\"","")}["results"][0]["now"]["${now}"]`);
           
   }
    //% block="WEATHER Mess [MESS]Day [DAY]Temp [TEMP]    " blockType="reporter" 
    //% MESS.shadow="string" MESS.defl="w1"  
    //% DAY.shadow="dropdown" DAY.options="DAY"
    //% TEMP.shadow="dropdown" TEMP.options="TEMP"
    export function Weather1(parameter: any, block: any) {
        let mess = parameter.MESS.code; 
        let day = parameter.DAY.code;  
        let temp = parameter.TEMP.code;
    
       Generator.addCode(`${mess.replace("\"","").replace("\"","")}["results"][0]["daily"][${day}]["${temp}"]`);
   }
    //% block="WEATHER  Mess[MESS] Brief [BRIEF]  " blockType="reporter" 
    //% MESS.shadow="string" MESS.defl="w1"  
    //% BRIEF.shadow="dropdown" BRIEF.options="BRIEF"   
    export function Weather3(parameter: any, block: any) {
        let mess = parameter.MESS.code; 
        let brief = parameter.BRIEF.code;  
             
        Generator.addCode(`${mess.replace("\"","").replace("\"","")}["results"][0]["suggestion"]["${brief}"]["brief"]`);
                  
   }
   }
    